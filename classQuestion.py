from typing import List
from random import shuffle


class SextException(Exception):
    def __init__(self, message="Unknown error occurred in Speed Exam Testing app"):
        self.message = message
        super().__init__(self.message)


class AllQuestionsExhaustedException(SextException):
    def __init__(self, message="There is no more questions"):
        self.message = message
        super().__init__(self.message)


#
# Class for representing question
# First answer is the correct one
#


class Question:
    def __init__(self, question: str):
        self.question: str = question
        self.answerList: List[str] = []
        self.ansCount: int = 0
        self.correctlyAns: int = 0
        self.incorrectlyAns: int = 0
        self.correctAnswer = None

    def correctIndex(self):
        for i, a in enumerate(self.answerList):
            if a == self.correctAnswer:
                return i

    def getQuestion(self):
        return self.question

    def addAnswer(self, answer: str):
        self.answerList.append(answer)
        self.correctAnswer = self.answerList[0]

    def isCorrect(self, potentialAnswer: str):
        return potentialAnswer == self.correctAnswer

    def getAnswers(self) -> List[str]:
        answers = self.answerList.copy()
        return answers

    def shuffleAnswers(self):
        shuffle(self.answerList)



class QuestionSet:
    def __init__(self):
        self.__listOfQ: List[Question] = []
        self.__pointer: int = 0
        self.count: int = 0
        self.correctlyAns: int = 0
        self.incorrectlyAns: int = 0

    def addQuestion(self, q: Question):
        self.__listOfQ.append(q)
        self.count += 1

    def getCurrentQuestion(self) -> Question:
        return self.__listOfQ[self.__pointer]

    def getQuestion(self) -> Question:
        if self.__pointer + 1 > self.count:
            raise AllQuestionsExhaustedException
        self.__pointer += 1
        return self.__listOfQ[self.__pointer - 1]

    def setList(self, new: List[Question]):
        self.__listOfQ = new

    def shuffleSet(self):
        shuffle(self.__listOfQ)

    def isLastQ(self):
        return self.__pointer == self.count

