from tkinter import *
from classQuestion import QuestionSet, Question
from classApp import questionDisplay
from questions import loadQuestions

TITLE = "Speed Exam Testing"
PATH = fr"""C:\Users\Patrik\Desktop\Projects\sext\sada.txt"""
ROOT = None
QDISPLAY = questionDisplay()
QSET = QuestionSet()

BGDEFAULT = 'white'
FGDEFAULT = 'black'
ANSX = 20
ANSY = 5
WRAP = 100

def showSummary(summaryButton):
    if summaryButton is not None:
        summaryButton.destroy()
    for b in QDISPLAY.getButtons():
        b.destroy()
    QDISPLAY.questionLabel.destroy()
    QDISPLAY.counterLabel.destroy()
    QDISPLAY.stopButton.destroy()
    scoreLabel = Label(text='Score: ' + str(QSET.correctlyAns) + '/' + str(QSET.incorrectlyAns + QSET.correctlyAns))
    scoreLabel.grid()

def resolveAnswer(question: Question, answer: int):
    if question.isCorrect(question.answerList[answer]):
        QSET.correctlyAns += 1
        question.correctlyAns += 1
        markCorrect(answer)
    else:
        question.incorrectlyAns += 1
        QSET.incorrectlyAns += 1
        markIncorrect(question, answer)
    if QSET.isLastQ():
        showSummaryButton()
    else:
        showNextButton()

    disableButtons()


def disableButtons():
    for b in QDISPLAY.getButtons():
        b.configure(command=empty)

def empty():
    pass

def enableButtons():
    q = QSET.getCurrentQuestion()
    for index, button in enumerate(QDISPLAY.getButtons()):
        button.configure(command= lambda: resolveAnswer(q, index))

def showSummaryButton():
    summaryButton = Button(ROOT, text='Summary', command=lambda: showSummary(summaryButton))
    summaryButton.grid(row=4, column=7)

def showNextButton():
    nextButton = Button(ROOT, text='Next question', wraplength=50)
    nextButton.grid(row=4, column=7)
    nextButton.configure(command=lambda: resolveNextQuestion(nextButton))

def resolveNextQuestion(nextButton):
    q = QSET.getQuestion()
    nextButton.destroy()
    QDISPLAY.counterLabel.configure(text='# ' + str(QSET.incorrectlyAns + QSET.correctlyAns))
    displayNextQuestion(q)


def markCorrect(ans: int):
    chosenButton = QDISPLAY.getButton(ans)
    chosenButton['bg'] = 'green'


def markIncorrect(q, ans: int):
    chosenButton = QDISPLAY.getButton(ans)
    chosenButton['bg'] = 'red'
    correctButton = QDISPLAY.getCorrectButton(q)
    correctButton['bg'] = 'green'


def stopButton():
    showSummary(None)

def displayInitial(q: Question):
    global QDISPLAY
    global QSET

    questionLabel = Label(ROOT, text=q.getQuestion())
    questionLabel.grid(row=2, column=3)
    QDISPLAY.questionLabel = questionLabel

    q.shuffleAnswers()
    a = Button(ROOT, text=q.answerList[0], wraplength=WRAP, justify=LEFT, height=ANSY, width=ANSX, command=lambda: resolveAnswer(q, 0))
    a.grid(row=6, column=0)
    b = Button(ROOT, text=q.answerList[1], wraplength=WRAP, justify=LEFT, height=ANSY, width=ANSX, command=lambda: resolveAnswer(q, 1))
    b.grid(row=6, column=5)
    c = Button(ROOT, text=q.answerList[2], wraplength=WRAP, justify=LEFT, height=ANSY, width=ANSX, command=lambda: resolveAnswer(q, 2))
    c.grid(row=8, column=0)
    d = Button(ROOT, text=q.answerList[3], wraplength=WRAP, justify=LEFT, height=ANSY, width=ANSX, command=lambda: resolveAnswer(q, 3))
    d.grid(row=8, column=5)

    QDISPLAY.setButtons([a, b, c, d])

    stop = Button(ROOT, text='STOP', command=stopButton)
    stop.grid(row=0, column=5)
    QDISPLAY.stopButton = stop

    counter = Label(ROOT, text='# ' + str(QSET.correctlyAns + QSET.incorrectlyAns))
    counter.grid(row=0, column=0)
    QDISPLAY.counterLabel = counter


def displayNextQuestion(q: Question):
    global QDISPLAY
    global QSET

    QDISPLAY.questionLabel.configure(text=q.getQuestion())

    q.shuffleAnswers()
    buts = QDISPLAY.getButtons()
    buts[0].configure(fg=FGDEFAULT, bg=BGDEFAULT, text=q.answerList[0], command=lambda: resolveAnswer(q, 0))
    buts[1].configure(fg=FGDEFAULT, bg=BGDEFAULT, text=q.answerList[1], command=lambda: resolveAnswer(q, 1))
    buts[2].configure(fg=FGDEFAULT, bg=BGDEFAULT, text=q.answerList[2], command=lambda: resolveAnswer(q, 2))
    buts[3].configure(fg=FGDEFAULT, bg=BGDEFAULT, text=q.answerList[3], command=lambda: resolveAnswer(q, 3))

def main():
    global ROOT
    global QSET
    ROOT = Tk()
    ROOT.title(TITLE)
    ROOT.geometry('700x400')
    QSET = loadQuestions(PATH)
    QSET.shuffleSet()
    q: Question = QSET.getQuestion()
    displayInitial(q)


    ROOT.mainloop()


main()
