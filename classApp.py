

class questionDisplay:
    def __init__(self):
        self.questionLabel=None
        self.ansButtons=[]
        self.__intToGetter = {0: self.getA, 1: self.getB,
                              2: self.getC, 3: self.getD}
        self.counterLabel = None
        self.stopButton = None

    def appendAnsButton(self, button):
        self.ansButtons.append(button)

    def setButtons(self, buttons):
        self.ansButtons = buttons

    def getButton(self, i: int):
        return self.__intToGetter[i]()

    def getA(self):
        return self.ansButtons[0]

    def getB(self):
        return self.ansButtons[1]

    def getC(self):
        return self.ansButtons[2]

    def getD(self):
        return self.ansButtons[3]

    def getCorrectButton(self, q):
        return self.getButton(q.correctIndex())

    def getButtons(self):
        return self.ansButtons
