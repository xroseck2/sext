from classQuestion import QuestionSet, Question

PATH = fr"""C:\Users\Patrik\Desktop\Projects\sext\sada.txt"""


def loadQuestions(path) -> QuestionSet:
    qSet = QuestionSet()
    isNewQ = True
    newQ = Question("Should never appear in testing")

    with open(path, "r") as f:
        for line in f.read().splitlines():
            if line == '':
                isNewQ = True
                continue

            if isNewQ:
                # Resolve new question
                newQ = Question(line)
                qSet.addQuestion(newQ)
                isNewQ = False
                continue

            newQ.addAnswer(line)

    return qSet
